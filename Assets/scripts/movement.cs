using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class movement : MonoBehaviour
{
    public float step;

    private Vector3 position;

    Vector2 startPos;
    float startTime;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        string swipe = getSwipe();

        if (swipe == "swipedRight")
        {
            if (transform.position.x<6)
            transform.position = transform.position + Vector3.right * step; ;
        }
        if (swipe == "swipedLeft")
        {
            if (transform.position.x>-6)
            transform.position = transform.position + Vector3.left * step;
        }
    }

    string getSwipe()
    {
        // If the touch is longer than MAX_SWIPE_TIME, we dont consider it a swipe
        const float MAX_SWIPE_TIME = 0.5f;

        // Factor of the screen width that we consider a swipe
        // 0.17 works well for portrait mode 16:9 phone
        const float MIN_SWIPE_DISTANCE = 0.17f;

        string swipeValue = "";

        if (Input.touches.Length > 0)
        {
            Touch t = Input.GetTouch(0);
            if (t.phase == TouchPhase.Began)
            {
                startPos = new Vector2(t.position.x / (float)Screen.width, t.position.y / (float)Screen.width);
                startTime = Time.time;
            }
            if (t.phase == TouchPhase.Ended)
            {
                if (Time.time - startTime > MAX_SWIPE_TIME) // press too long
                    return "";

                Vector2 endPos = new Vector2(t.position.x / (float)Screen.width, t.position.y / (float)Screen.width);

                Vector2 swipe = new Vector2(endPos.x - startPos.x, endPos.y - startPos.y);

                if (swipe.magnitude < MIN_SWIPE_DISTANCE) // Too short swipe
                    return "";

                if (Mathf.Abs(swipe.x) > Mathf.Abs(swipe.y))
                { 
                    if (swipe.x > 0)
                    {
                        swipeValue = "swipedRight";
                    }
                    else
                    {
                        swipeValue = "swipedLeft";
                    }
                }
                else
                { 
                    if (swipe.y > 0)
                    {
                        swipeValue = "swipedUp";
                    }
                    else
                    {
                        swipeValue = "swipedDown";
                    }
                }
            }
        }

        return swipeValue;

    }
}
